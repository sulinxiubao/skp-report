package com.skong.report.service;

import com.skong.report.entity.ViewUreportEntity;

public interface ViewUreportService {
    int insertOne(ViewUreportEntity viewUreportEntity);
    String queryNameByID(String id);
}