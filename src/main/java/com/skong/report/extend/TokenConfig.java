package com.skong.report.extend;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.jwt.config.encryption.SecretEncryptionConfiguration;
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration;
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator;
import org.pac4j.jwt.profile.JwtGenerator;


public class TokenConfig {

    //请求头
    public static final String tokenHeader="x-access-token";

    //jwt秘钥
    private static final String salt = "11112222333344445555666677778888";

    private static JwtAuthenticator jwtAuthenticator;

    public static JwtAuthenticator getJwtAuthenticator(){
        if(jwtAuthenticator==null){
            jwtAuthenticator = new JwtAuthenticator();
            jwtAuthenticator.addSignatureConfiguration(new SecretSignatureConfiguration(salt));
            jwtAuthenticator.addEncryptionConfiguration(new SecretEncryptionConfiguration(salt));
        }
        return jwtAuthenticator;
    }

    public static JwtGenerator getJwtGenerator(){
        return new JwtGenerator(new SecretSignatureConfiguration(salt), new SecretEncryptionConfiguration(salt));
    }

    public static CommonProfile validateToken(String token){
        return getJwtAuthenticator().validateToken(token);
    }
}
