package com.skong.report.mapper;

import com.skong.report.entity.ViewUreportEntity;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ViewUreportMapper {

    public String queryNameByID(String id);
    public int insertOne(ViewUreportEntity viewUreportEntity);
}
