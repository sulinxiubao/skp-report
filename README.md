# skp-report

#### 介绍
基于UReport2进行二次拓展，以支持SpringCloud

由于目前实行的Saas化业务系统，各租户都有通用的以及个性的报表需求。而这些报表需求如果按照传统的开发模式，会出现研发周期长的问题；且部分用户会提出短期的数据报表需求，并且会反复变更。这更加加大了整个研发成本

所以我们寻找了相关的替代方案用于解决该问题，此处使用了UReport2

> 适用场景

针对客户需求，完成数据简单（靠SQL可完成）对UI要求不高（缺少绚丽报表）的基础数据呈现工作，用于报表展示。

不适用以下场景：

- 高度自由化的报表呈现（复杂布局）
- 需要深度挖掘统计的数据（大数据）
- 数据大屏展示（UI绚丽）

PS: 以上内容会与Ant-design的UI存在差异

> 可实现效果

- 查询表格
- 统计表格
- 统计图表（柱状/折线等等）
- 下载打印

教程: https://www.w3cschool.cn/ureport/

参考文档：
https://www.jianshu.com/p/6523f0309255
https://blog.csdn.net/qq_35170213/article/details/80287890
https://www.cnblogs.com/Seven-cjy/p/9542616.html

本文除简单讲解在SpringBoot+SpringCloud集成外，会额外讲解最终业务应用场景。主要包含以下

- 个人使用的案例分享
- 报表引擎在前端React项目应用
- 自定义xml存储
- 租户下如何应用报表引擎

#### 软件架构

依赖组件 | 依赖版本
---|---
SpringBoot | 1.5.6.RELEASE 
SpringCloud | Edgware.RELEASE 
mysql-connector-java | 8.0.16 
spring-boot-starter-data-redis | 1.5.9.RELEASE
ureport2-console | 2.2.9
org.pac4j | 3.0.2


#### 安装教程

1.  git clone
2.  mvn package
3.  执行sql
4、 修改配置文件中的`application.properties`中mysql / redis连接，或者直接连接config中心
5.  java -jar skp-report.jar
6.  访问http://127.0.0.1:8769/ureport/designer打开设计器

#### 使用说明

1.  设计器：http://127.0.0.1:8769/ureport/designer
2.  报表页：http://127.0.0.1:8769/ureport/preview?_u=report-price.ureport.xml&orderCode=432432&orgCode=0023
3.  用户信息：通过`TokenFilter`拦截token，调用微服务转换为详细信息

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

更多技巧访问博客

参考https://segmentfault.com/a/1190000021771780
